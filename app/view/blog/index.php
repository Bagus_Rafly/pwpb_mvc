<div class="container mt-5">
    <div class="row">
        <div class="col-lg-6">
            <?php Flasher::flash(); ?>
        </div>
    </div>
    <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#formModal">
        Buat Artikel
    </button>
    <div class="row">
        <?php foreach ($data['blog'] as $blog) : ?>
            <div class="col-md-4 mb-3">
                <div class="card mb-3 shadow" style="max-width: 540px;">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="<?=BASEURL;?>/uploads/<?= $blog['gambar_path'];?>" class="img-fluid rounded-start " alt="..." style="height: 12rem; object-fit:cover;">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= $blog['judul']; ?></h5>
                                <p class="card-text">
                                    <strong>Penulis:</strong> <?= $blog['penulis']; ?><br>
                                    <strong>Tulisan:</strong> <?= $blog['tulisan']; ?>
                                </p>
                                <a class="btn btn-primary" href="<?= HREF ?>blog/detail/<?= $blog['id']; ?>">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="judulModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="judulModal">Buat Artikel</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="<?= HREF; ?>blog/tambah" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="judul">Judul Artikel</label>
                            <input type="text" class="form-control" id="judul" name="judul" required>
                        </div>
                        <div class="form-group">
                            <label for="tulisan">Isi Artikel</label>
                            <textarea class="form-control" id="tulisan" rows="5" name="tulisan" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="penulis">Nama Penulis</label>
                            <input type="text" class="form-control" id="penulis" name="penulis" required>
                        </div>
                        <div class="form-group">
                            <label for="gambar">Gambar</label>
                            <input type="file" class="form-control" id="gambar" name="gambar" required>
                            <img class="img-fluid mt-3" src="" id="preview" alt="">
                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Tambah Artikel</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    

</div>
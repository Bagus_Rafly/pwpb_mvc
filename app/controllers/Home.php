<?php
class Home extends Controller {

    public function __construct() {
        if (!$_SESSION['login']) {
            header('Location: ' . HREF . 'auth/login');
        }
    }

    public function index() {
        $data['judul'] = "Home";
        $data['home-active'] = true;
        $this->view('template/header', $data);
        $this->view('home/index',$data);
        $this->view('template/footer');
    }
}


?>

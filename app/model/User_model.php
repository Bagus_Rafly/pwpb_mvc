<?php

class User_model
{
    private $db;
    private $table = 'user';

    public function __construct()
    {
        $this->db = new Database;
    }
    public function register($data){
        $password = $data['password'];
        $query = 'INSERT INTO ' . $this->table . '(email,nama,Password) VALUES(:email,:nama,:Password)';
        $hashedPass = password_hash($password, PASSWORD_DEFAULT);
        $this->db->query($query);
        $this->db->bind('email', $data['email']);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('Password', $hashedPass);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function login($data){
        $email = $data['email'];
        $query = 'SELECT * FROM ' . $this->table . ' WHERE email = :email';
        $this->db->query($query);
        $this->db->bind('email', $email);
        $user = $this->db->resultSingle();
        $passPost = $data['Password'];
        $passUser = $user['Password'];
        if(password_verify($passPost, $passUser)){
            $_SESSION['login']=true;
            return 1;
        }else{
            return -1;
        }

    }

}

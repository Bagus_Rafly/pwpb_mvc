<?php
class Blog extends Controller {

    public function __construct() {
        if (!$_SESSION['login']) {
            header('Location: ' . HREF . 'auth/login');
        }
    }

    public function index() {
        $data['judul'] = "Blog";
        $blogs = $this->model("Blog_model")->getAllBlog();
        
        // Memotong teks pada setiap blog menjadi 100 karakter
        foreach ($blogs as &$blog) {
            $blog['tulisan'] = $this->limitText($blog['tulisan'], 80);
        }

        $data['blog'] = $blogs;
        $data['blog-active'] = true;

        $this->view('template/header', $data);
        $this->view('blog/index', $data);
        $this->view('template/footer');
    }

    // Fungsi untuk memotong teks
    private function limitText($text, $limit) {
        if (strlen($text) > $limit) {
            $text = substr($text, 0, $limit) . '...';
        }
        return $text;
    }

    public function detail ($id) {
        $data['judul'] = "Detail Blog";
        $data['blog'] = $this->model("Blog_model")->getBlogById($id);
        $this->view('template/header', $data);
        $this->view('blog/detail', $data);
        $this->view('template/footer');
    }

    public function tambah() {
        // Upload image and get the path
        $gambarPath = $this->uploadImage($_FILES['gambar']);
    
    
        // Add image path to the $_POST array
        $_POST['gambar_path'] = $gambarPath;
    
        if ($this->model('Blog_model')->buatArtikel($_POST) > 0) {
            Flasher::setFlash('berhasil', 'ditambahkan', 'success');
            header('Location: ' . HREF . 'blog');
            exit;
        } else {
            Flasher::setFlash('gagal', 'ditambahkan', 'danger');
            header('Location: ' . HREF . 'blog');
        }
    }
    private function uploadImage($file) {
        $uploadDirectory = 'public/uploads/'; 
    
        // Generate a unique filename to avoid overwriting existing files
        $filename = uniqid() . '_' . basename($file['name']);
    
        $targetPath = $uploadDirectory . $filename;
    
        // Move the uploaded file to the specified directory
        if (move_uploaded_file($file['tmp_name'], $targetPath)) {
            return $filename; // Return the path to the uploaded image
        } else {
            return false; // Return false on upload failure
        }
    }
    
    
    public function hapus($id) {
        $imagePath = $this->model('Blog_model')->getImagePath($id);
    
        if ($this->model('Blog_model')->hapusArtikel($id) > 0) {
            if ($imagePath) {
                unlink($imagePath);
            }
    
            Flasher::setFlash('berhasil', 'dihapus', 'success');
            header('Location: ' . HREF . 'blog');
            exit;
        } else {
            Flasher::setFlash('gagal', 'dihapus', 'danger');
            header('Location: ' . HREF . 'blog');
        }
    }
    

    public function update($id) {
        $data['judul'] = "Update Artikel";
        $data['blog'] = $this->model("Blog_model")->getBlogById($id);
        $this->view('template/header', $data);
        $this->view('blog/update', $data);
        $this->view('template/footer');
    }

    public function prosesUpdate($id) {
        if ($this->model('Blog_model')->updateArtikel($id, $_POST) > 0) {
            Flasher::setFlash('berhasil', 'diupdate', 'success');
            header('Location: ' . HREF . 'blog');
            exit;
        } else {
            Flasher::setFlash('gagal', 'diupdate', 'danger');
            header('Location: ' . HREF . 'blog/update/' . $id);
        }
    }

    
}
?>

<?php

class Blog_model {
    private $table = 'blog';
    private $db;

    public function __construct() {
        $this->db = new Database;
    }

    public function getAllBlog() {
        $this->db->query("SELECT * FROM " . $this->table);
        return $this->db->resultAll();
    }

    public function getBlogById($id) {
        $this->db->query("SELECT * FROM " . $this->table . " WHERE id=:id");
        $this->db->bind('id', $id);
        return $this->db->resultSingle();
    }

    public function buatArtikel($data) {
        $query = "INSERT INTO blog (judul, tulisan, penulis, gambar_path) VALUES (:judul, :tulisan, :penulis, :gambar_path)";
        $this->db->query($query);
        $this->db->bind('judul', $data['judul']);
        $this->db->bind('tulisan', $data['tulisan']);
        $this->db->bind('penulis', $data['penulis']);
        $this->db->bind('gambar_path', $data['gambar_path']);   
        $this->db->execute();
        return $this->db->rowCount();
    }
    
    
    public function hapusArtikel($id) {
        $imagePath = $this->getImagePath($id);

        $query = "DELETE FROM blog WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();

    if ($imagePath) {
        unlink($imagePath);
    }

    return $this->db->rowCount();

}   

    // Helper method to get the image path
    public function getImagePath($id) {
        $query = "SELECT gambar_path FROM blog WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id', $id);
        $result = $this->db->resultSingle();

        return isset($result['gambar_path']) ? $result['gambar_path'] : null;

    }

    public function updateArtikel($id, $data) {
        $query = "UPDATE blog SET judul=:judul, tulisan=:tulisan, penulis=:penulis WHERE id=:id";
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->bind('judul', $data['judul']);
        $this->db->bind('tulisan', $data['tulisan']);
        $this->db->bind('penulis', $data['penulis']);
        $this->db->execute();
        return $this->db->rowCount();
    }
}

?>

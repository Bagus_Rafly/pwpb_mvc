<?php
    class User extends Controller {
        public function __construct() {
            if (!$_SESSION['login']) {
                header('Location: ' . HREF . 'auth/login');
            }
        }
    
        public function index() {
            $data['judul'] = 'User';
            $data['user-active'] = true;
            $this->view('template/header', $data);
            $this->view('user/index');
            $this->view('template/footer');  
        }
    
        public function profile($nama = 'Rapli', $pekerjaan = 'Pengangguran') {
            $data['judul'] = 'User';
            $data['nama'] = $nama;
            $data['pekerjaan'] = $pekerjaan;
            $this->view('template/header', $data);
            $this->view('user/profile', $data);
            $this->view('template/footer');
        }
    }
    

    

    
<div class="container mt-5 p-5 bg-light mb-3 shadow" style="height: 100%;">
    <div class="text-center">
        <h1 class="mt-3 display-4"><?= $data['blog']['judul']; ?></h1>
        <img src="<?=BASEURL;?>/uploads/<?= $data['blog']['gambar_path'];?>" class="img-fluid" style="" alt="...">
        <h6 class="card-subtitle mb-2 text-muted mt-3">dibuat oleh <?= $data['blog']['penulis']; ?></h6>
        <p class="text-secondary">Created at <?= date('d F Y', strtotime($data['blog']['created_at'])); ?></p>
        <hr>
        <p class=""><?= $data['blog']['tulisan']; ?></p>
            <a href="<?= HREF; ?>blog" class="btn btn-success mt-3 mb-2">Kembali</a>
            <a href="<?= HREF ?>blog/hapus/<?= $data['blog']['id'] ?>" class="btn btn-danger mt-3 mb-2">Hapus Blog</a>
            <a href="" class="btn btn-info mt-3 mb-2">Edit file</a>
    </div>
    
</div>

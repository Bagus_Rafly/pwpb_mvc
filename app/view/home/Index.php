<div class="container mt-5 p-5 bg-white" style="height: 50vh;">
  <div class="row">
    <div class="col-md-6">
      <img src="https://i.pinimg.com/564x/7e/80/fb/7e80fba262b03c2524e8e0a34c4c771d.jpg" class="rounded img-fluid" alt="Image description">
    </div>
    <div class="col-md-6">
      <h1 class="mt-3 display-4">Yokoso! ようこそ</h1>
      <p class="">Selamat datang di blog pribadi saya!</p>
      <hr>
      <p class="fw-bold">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquam recusandae hic labore,
        quibusdam nam voluptatum.</p>
      <button class="btn btn-primary mt-3">Learn More</button>
    </div>
  </div>
</div>

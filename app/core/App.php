<?php

class App {
    protected $controller = "Home";
    protected $method = "index";
    protected $params = [];

    public function __construct() {
        $url = $this->parseUrl();

        $this->setController($url);
        $this->loadController();

        $this->setMethod($url);
        $this->loadMethod();

        $this->params = array_values($url);

        $this->executeControllerMethod();
    }

    protected function setController(&$url) {
        if (!empty($url[0])) {
            $controllerName = ucfirst(strtolower($url[0]));

            if ($this->controllerExists($controllerName)) {
                $this->controller = $controllerName;
                unset($url[0]);
            }
        }
    }

    protected function controllerExists($controllerName) {
        $filePath = "app/controllers/{$controllerName}.php";
        return file_exists($filePath);
    }

    protected function loadController() {
        require_once "app/controllers/{$this->controller}.php";
        $controllerClass = $this->controller;
        $this->controller = new $controllerClass;
    }
    

    protected function setMethod(&$url) {
        if (!empty($url[1]) && method_exists($this->controller, $url[1])) {
            $this->method = $url[1];
            unset($url[1]);
        }
    }

    protected function loadMethod() {
        // No need to explicitly load the method.
    }

    protected function executeControllerMethod() {
        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    protected function parseUrl() {
        $url = $_SERVER['REQUEST_URI'];
        $url = trim($url, '/');
        $url = rtrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $url = explode("/", $url);
        array_shift($url);

        return $url;
    }
}


?>

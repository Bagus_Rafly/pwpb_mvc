<div class="container ">
  <h5>Form Login</h5>
  <form action="<?= HREF; ?>auth/tryLogin" method="post">
    <div class="mb-3">
      <label for="email" class="form-label">Gmail</label>
      <input type="email" class="form-control" id="email" name="email" required>
    </div>
    <div class="mb-3">
      <label for="password" class="form-label">Password</label>
      <input type="password" class="form-control" id="password" name="Password" required>
    </div>
    <button type="submit" class="btn btn-primary">Login</button>
  </form>
</div>
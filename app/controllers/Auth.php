<?php

class Auth extends Controller
{
    public function login(){
        $data['judul'] = 'Login';
        $this->view('template/header' );
        $this->view('auth/login');
        $this->view('template/footer');

        if (isset ($_SESSION['login']) == true) {
            header('Location: ' . HREF . '');
        }
    }

    public function register(){
        $this->view('template/header' );
        $this->view('auth/register');
        $this->view('template/footer');
    }

    public function tryRegister(){
        $this->model('User_model')->register($_POST);
        header('Location:' . HREF .'auth/login');

    }

    public function tryLogin(){
        if($this->model('User_model')->login($_POST) > 0){
            header('Location:' . HREF . 'home/index');
        }else{
            header('Location:' . HREF . 'auth/login');
        }
    }

    public function logout(){
        session_unset();
        session_destroy();
        header('Location: '. HREF .'auth/login');
        exit;
    }

}
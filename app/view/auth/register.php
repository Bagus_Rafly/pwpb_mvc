<div class="container ">
  <h5>Form Register</h5>
  <form action="<?= HREF; ?>auth/tryRegister" method="post">
    <div class="mb-3">
      <label for="email" class="form-label">Gmail</label>
      <input type="email" class="form-control" id="email" name="email" required>
    </div>
    <div class="mb-3">
      <label for="nama" class="form-label">Nama</label>
      <input type="text" class="form-control" id="nama" name="nama" required>
    </div>
    <div class="mb-3">
      <label for="password" class="form-label">Password</label>
      <input type="password" class="form-control" id="password" name="password" required>
    </div>
    <button type="submit" class="btn btn-primary">Register</button>
  </form>
</div>